import { createVuetify } from 'vuetify'
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import fr from '@/locales/fr.json'
import en from '@/locales/en.json'

const vuetify = createVuetify({
  components,
  directives,
  locale: {
    locale: 'fr',
    fallback: 'en',
    messages: { fr, en },
  },
})

export default vuetify